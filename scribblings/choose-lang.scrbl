#lang scribble/manual

@require[
  scribble/example
  scribble-abbrevs/manual
  (for-label racket/base
             (only-in racket/contract/base -> any/c)
             choose-lang)]

@title{choose-lang: Pick #lang at read-time}

@defmodulelang[choose-lang]{
  Meta-language for selecting a @hash-lang[] as the module is initially read.
}

For example, the following module raises a type error if the environment
variable @litchar{CHOOSE_TYPES} is set, and otherwise ignores the type
annotation.

@codeblock|{
#lang choose-lang (if (getenv "CHOOSE_TYPES") typed/racket typed/racket/no-check)

(: f Integer)
(define f 'foo)
}|
