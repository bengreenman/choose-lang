#lang racket/base

(provide
  (rename-out
   [-read read]
   [-read-syntax read-syntax]
   [-get-info get-info]))

(require
  syntax/readerr
  racket/match
  (only-in syntax/module-reader
    make-meta-reader))

;; =============================================================================

(define (read-choose-out-expr in)
  (read in))

(define (eval-choose-out-expr v)
  (match v
   [(? symbol? sym)
    sym]
   [`(if ,tst ,(? symbol? sym0) ,(? symbol? sym1))
    (if (eval-choose-out-test tst)
      sym0
      sym1)]
   [_
     (raise-argument-error 'choose-out "choose-out-expr?" v)]))

(define (eval-choose-out-test v)
  (match v
   [`(getenv ,(? string? var))
    (and (getenv var) #true)]
   [_
     (raise-argument-error 'choose-out "choose-out-test?" v)]))

(define (symbol->language-path sym)
  (and (module-path? sym)
       (vector
        ;; try submod first:
        `(submod ,sym reader)
        ;; fall back to /lang/reader:
        (string->symbol (string-append (symbol->string sym) "/lang/reader")))))

(define-values [-read -read-syntax -get-info]
  (make-meta-reader
    'choose-lang
    "choose-lang-expr"
    #:read-spec read-choose-out-expr
    #;module-path-parser (compose1 symbol->language-path eval-choose-out-expr)
    #;convert-read values
    #;convert-read-syntax values
    #;convert-get-info values))
